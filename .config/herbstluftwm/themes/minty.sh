#!/usr/bin/env bash

hc set frame_border_active_color '#4271ae'
hc set frame_border_normal_color '#2f2f2f'
hc set frame_bg_normal_color '#2f2f2f'
hc set frame_bg_active_color '#4271ae'
hc set frame_border_width 3
hc set always_show_frame 0
hc set frame_bg_transparent 1
hc set frame_transparent_width 0
hc set frame_gap 15
hc set default_frame_layout 2
hc set window_gap 0
hc set frame_padding 0
hc set smart_window_surroundings 1
hc set smart_frame_surroundings 1
hc set mouse_recenter_gap 0
hc set focus_follows_mouse 1
hc set tree_style '╾│ ├└╼─┐'
# hc set swap_monitors_to_get_tag 0 #Switch to monitor where tag is displayed

hc attr theme.active.color '#4271ae'
hc attr theme.normal.color '#2f2f2f'
hc attr theme.urgent.color orange
hc attr theme.inner_width 0
hc attr theme.inner_color "#2f2f2f"
hc attr theme.border_width 2
hc attr theme.floating.border_width 3
hc attr theme.floating.outer_width 1
hc attr theme.floating.outer_color "#1a1a1a"
hc attr theme.active.inner_color '#4271ae'
hc attr theme.active.outer_color '#4271ae'
hc attr theme.background_color '#141414'
