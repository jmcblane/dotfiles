source /home/jake/.config/nvim/plugs.vim

let g:PaperColor_Theme_Options = { 'theme': 
            \ { 'default.dark': 
            \ { 'transparent_background': 1
            \ } } }
" let g:lightline = { 'colorscheme': 'newsolar' }

let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240
autocmd! User GoyoEnter set textwidth=70
autocmd! User GoyoEnter PencilHard
autocmd! User GoyoLeave set textwidth=0
autocmd! User GoyoLeave PencilOff
autocmd! User GoyoLeave hi EndOfBuffer ctermfg=236

let g:vimwiki_list = [{'path': '~/docs/vimwiki', 'auto_toc': 1, 'syntax': 'markdown', 'ext': '.md'}]
autocmd FileType vimwiki hi VimwikiHeader1 ctermfg=15 cterm=bold
autocmd FileType vimwiki hi VimwikiHeader2 ctermfg=4 cterm=bold
autocmd FileType vimwiki hi VimwikiHeader3 ctermfg=2 cterm=bold
autocmd FileType vimwiki hi VimwikiHeader4 ctermfg=5 cterm=bold
autocmd FileType vimwiki hi VimwikiLink ctermfg=110
autocmd FileType vimwiki hi VimwikiList ctermfg=4

let &showbreak=" "
let g:ctrlp_show_hidden = 1

colorscheme PaperColor

autocmd FileType tex nmap <buffer> <F5> :!latexmk -pdf %<CR>
autocmd FileType rust nmap <buffer> <F4> :!rustc -o test %<CR>
autocmd FileType rust nmap <buffer> <F5> :term cargo run<CR>
autocmd FileType markdown nmap <buffer> <F5> :!pandoc % -o %:r.pdf --variable=geometry:margin=0.5in<CR>
autocmd BufRead /tmp/rtv_* set syntax=markdown
autocmd BufRead /tmp/rtv_* SoftPencil
autocmd BufRead /tmp/tuir_* set syntax=markdown
autocmd BufRead /tmp/tuir_* SoftPencil

autocmd BufNewFile *.py 0r ~/.config/nvim/snippets/python.py
autocmd BufNewFile *.sh 0r ~/.config/nvim/snippets/shell.sh
autocmd BufNewFile *.rb 0r ~/.config/nvim/snippets/ruby.rb

set bg=dark
set mouse=a
" set guicursor=
set noshowmode
set timeoutlen=400
set encoding=utf8
set si
set bri
set wrap
set linebreak
set nolist
set tabstop=2 softtabstop=2 expandtab shiftwidth=2
set nocursorline
set wildignore=*.o,*~,*.pyc
set hid
set ignorecase
set smartcase
set lazyredraw
set backspace=eol,start,indent
set whichwrap+=<,>,h,l
set magic
set showmatch
set mat=2
set noerrorbells
set novisualbell
set number
set relativenumber
set stal=2

filetype plugin on
filetype indent on
syntax on

map gt :tabn<CR>
map gT :tabp<CR>
map T  :tabnew<CR>
map gb :bn<CR>
map gB :bp<CR>
map wc :close<CR>
map ws :split<CR>
map wv :vsplit<CR>
map wo :CommandT<CR>
map <leader><space> :noh<CR>
map <leader>oa :e ~/voidpi/www/thinkspace/articles.md<CR>:e ~/.news_cache<CR>
map <leader>on :e ~/docs/news_feed.xml<CR>
map <leader>hi :so $VIMRUNTIME/syntax/hitest.vim<CR>
map <leader>go :Goyo<CR>
map <leader>rn :set invrelativenumber<CR>
map <leader>c :checktime<CR>
nnoremap <leader>q :bd<CR>

hi Folded ctermfg=2 ctermbg=8
hi FoldColumn ctermfg=2
hi Todo ctermfg=1 ctermbg=236
hi Underlined ctermfg=6
hi EndOfBuffer ctermfg=236

" let g:lightline = { 'colorscheme': '16color' }
" let g:lightline = { 'colorscheme': '16color2' }
" let g:lightline = { 'colorscheme': 'OldHope' }
" let g:lightline = { 'colorscheme': 'PaperColor' }
" let g:lightline = { 'colorscheme': 'PaperColor_dark' }
" let g:lightline = { 'colorscheme': 'PaperColor_light' }
" let g:lightline = { 'colorscheme': 'Tomorrow' }
" let g:lightline = { 'colorscheme': 'Tomorrow_Night' }
" let g:lightline = { 'colorscheme': 'Tomorrow_Night_Blue' }
" let g:lightline = { 'colorscheme': 'Tomorrow_Night_Bright' }
" let g:lightline = { 'colorscheme': 'Tomorrow_Night_Eighties' }
let g:lightline = { 'colorscheme': 'darcula' }
" let g:lightline = { 'colorscheme': 'default' }
" let g:lightline = { 'colorscheme': 'deus' }
" let g:lightline = { 'colorscheme': 'jellybeans' }
" let g:lightline = { 'colorscheme': 'landscape' }
" let g:lightline = { 'colorscheme': 'materia' }
" let g:lightline = { 'colorscheme': 'material' }
" let g:lightline = { 'colorscheme': 'molokai' }
" let g:lightline = { 'colorscheme': 'newsolar' }
" let g:lightline = { 'colorscheme': 'newsolarblue' }
" let g:lightline = { 'colorscheme': 'newsolarred' }
" let g:lightline = { 'colorscheme': 'nord' }
" let g:lightline = { 'colorscheme': 'one' }
" let g:lightline = { 'colorscheme': 'powerline' }
" let g:lightline = { 'colorscheme': 'powerlineish' }
" let g:lightline = { 'colorscheme': 'seoul256' }
" let g:lightline = { 'colorscheme': 'solarized' }
" let g:lightline = { 'colorscheme': 'srcery_drk' }
" let g:lightline = { 'colorscheme': 'wombat' }

let g:lightline#bufferline#enable_devicons=1
let g:lightline#bufferline#show_number=1
let g:lightline.tabline = {'left': [['buffers']], 'right': [['close']]}
let g:lightline.component_expand = {'buffers': 'lightline#bufferline#buffers'}
let g:lightline.component_type   = {'buffers': 'tabsel'}
