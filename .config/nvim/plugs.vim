call plug#begin('~/.config/nvim/plugged')
Plug 'itchyny/lightline.vim'
Plug 'mengelbrecht/lightline-bufferline'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-commentary'
Plug 'reedes/vim-colors-pencil'
Plug 'easymotion/vim-easymotion'
Plug 'NLKNguyen/papercolor-theme'
Plug 'christoomey/vim-tmux-navigator'
Plug 'reedes/vim-pencil'
Plug 'tpope/vim-surround'
Plug 'zah/nim.vim'
" Plug 'dbeniamine/cheat.sh-vim'
Plug 'junegunn/goyo.vim'
" Plug 'junegunn/limelight.vim'
Plug 'jamessan/vim-gnupg'
" Plug 'ryanoasis/vim-devicons' " Love this, but... let's be purer
" Plug 'dag/vim-fish'
" Plug 'tpope/vim-vinegar'
" Plug 'davidhalter/jedi-vim'
" Plug 'wincent/command-t'
" Plug 'neovim/neovim-ruby'
" Plug 'junegunn/goyo.vim'
" Plug 'junegunn/fzf.vim'
" Plug '/usr/bin/fzf'
" Plug 'baabelfish/nvim-nim'
call plug#end()
