local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
local beautiful = require("beautiful")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget
require("awful.hotkeys_popup.keys")

-- Error Testing -----------------------------------------------------

if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end

----------------------------------------------------------------------

beautiful.init( "/home/jake/.config/awesome/themes/mytheme/theme.lua" )

terminal = "gnome-terminal -e /home/jake/scripts/tmux/new-session.sh"
editor = os.getenv("EDITOR")
editor_cmd = "gnome-terminal -e " .. editor
modkey = "Mod1"
networks = {"wlp2s0"}

awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.floating,
    awful.layout.suit.max,
    awful.layout.suit.fair.horizontal,
}

menubar.utils.terminal = terminal

awful.tag.add("tile", {
  screen    =   s,
  selected  =   true,
  layout    =   awful.layout.layouts[1],
  master_width_factor = 0.5 } )

awful.tag.add("float", {
  screen    =   s,
  layout    =   awful.layout.layouts[3] } )

awful.tag.add("max", {
  screen    =   s,
  layout    =   awful.layout.layouts[4] } )

awful.tag.add("xtra", {
  layout    = awful.layout.layouts[5] })

globalkeys = gears.table.join(
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    awful.key({ modkey }, "j", function () awful.client.focus.bydirection("down")
        if client.focus then client.focus:raise() end
    end),
    awful.key({ modkey }, "k", function () awful.client.focus.bydirection("up")
        if client.focus then client.focus:raise() end
    end),
    awful.key({ modkey }, "h", function () awful.client.focus.bydirection("left")
        if client.focus then client.focus:raise() end
    end),
    awful.key({ modkey }, "l", function () awful.client.focus.bydirection("right")
        if client.focus then client.focus:raise() end
    end),

    awful.key({ modkey, "Shift" }, "j", function () awful.client.swap.bydirection("down")
    end),
    awful.key({ modkey, "Shift" }, "k", function () awful.client.swap.bydirection("up")
    end),
    awful.key({ modkey, "Shift" }, "h", function () awful.client.swap.bydirection("left")
    end),
    awful.key({ modkey, "Shift" }, "l", function () awful.client.swap.bydirection("right")
    end),

   awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),

    awful.key({ modkey, "Shift"   }, "Return", function () awful.spawn(terminal) end,
              {description = "open default terminal", group = "launcher"}),
    awful.key({ "Mod4", "Shift"   }, "Return", function () awful.spawn("gnome-terminal") end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ "Mod4",   }, "w", function () awful.spawn("gnome-terminal -- /home/jake/scripts/weelaunch.sh") end,
              {description = "show weechat tmux session", group = "launcher"}),
    awful.key({ modkey, "Shift" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey, "Control"   }, "l",     function () awful.tag.incmwfact( 0.0275)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey, "Control"   }, "h",     function () awful.tag.incmwfact(-0.0275)          end,
              {description = "decrease master width factor", group = "layout"}),

    awful.key({ modkey, "Control"   }, "j",     function () awful.client.incwfact( 0.0275)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey, "Control"   }, "k",     function () awful.client.incwfact(-0.0275)          end,
              {description = "decrease master width factor", group = "layout"}),

    awful.key({ modkey,   }, ".",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey,   }, ",",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),

    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({   }, "XF86MonBrightnessUp", function () awful.spawn("xbacklight -inc 10") end,
              {description = "increase brightness"}),

    awful.key({   }, "XF86MonBrightnessDown", function () awful.spawn("xbacklight -dec 10") end,
              {description = "decrease brightness"}),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                      client.focus = c
                      c:raise()
                  end
              end,
              {description = "restore minimized", group = "client"}),

    awful.key({ modkey }, "p", function() awful.spawn("dmenu_run -p 'Run:'") end,
    {description = "launch dmenu"}),

    awful.key({ modkey }, "F10", function() awful.spawn("/home/jake/scripts/nkjv/nkjv-menu.sh") end,
    {description = "launch nkjv-menu"}),

    awful.key({ modkey }, "F11", function() awful.spawn("/home/jake/scripts/nkjv/nkjv-menu.sh md") end,
            {description = "launch markdown nkjv-menu"}),

    awful.key({ modkey }, "F12", function() awful.spawn("/home/jake/scripts/nkjv/nkjv-menu.sh marp") end,
            {description = "launch marp nkjv-menu"}),

    awful.key({ modkey }, "r", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"})
)

clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey,           }, "t",  function (c)
        c.floating = not c.floating
        c.ontop = not c.ontop
    end,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey,           }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey, "Shift" }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),

    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}),

    awful.key({ modkey, "Shift" }, "m",
        function (c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end ,
        {description = "(un)maximize vertically", group = "client"}),

    awful.key({ modkey, "Control"   }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end ,
        {description = "(un)maximize horizontally", group = "client"})
)

for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),

        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),

        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),

        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))


root.keys(globalkeys)

awful.rules.rules = {
    { rule = { }, -- All Clients
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen,
                     size_hints_honor = false
     },
     callback = awful.client.setslave
    },

    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "xfce4-panel",
          "Discord Updater",
          "Ulauncer",
          "nemo"
        },
        class = {
          "Arandr",
          "Gpick",
          "Kruler",
          "MessageWin",
          "Sxiv",
          "Wpa_gui",
          "pinentry",
          "veromix",
          "xtightvncviewer"},
        name = {
          "Event Tester",  -- xev.
          "Customize Look and Feel", -- lxappearance
          "Whisker Menu",
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    { rule_any = {name = { "polybar" }
      }, properties = { dockable = true, skip_taskbar = true, type = dock, border_width = 0, focusable = false, sticky = true }
    },
    { rule_any = {name = { "xfce4-panel", "synapse", "Ulauncher", "nemo-desktop" },
    class = {"InputOutput", "xfce4-panel", "polybar"},
      }, properties = { border_width = 0 }
    },
    { rule_any = {name = { "Whisker Menu" },
      }, properties = { titlebars_enabled = false }
    },
    { rule_any = {name = { "xfce4-panel", "polybar" },
      }, properties = { skip_taskbar = true }
    },
}

client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        awful.placement.no_offscreen(c)
    end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
        and awful.client.focus.filter(c) then
        client.focus = c
    end
end)

awful.spawn("/home/jake/.config/polybar/launch.sh xmonad")
awful.spawn("nitrogen --restore")
-- awful.spawn("compton")

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
