local themes_path = require("gears.filesystem").get_themes_dir()
local dpi = require("beautiful.xresources").apply_dpi
local theme = {}

theme.font = "Inconsolata Nerd Font 9"

theme.fg_normal  = "#cdcdce"
theme.fg_focus   = "#ffffff"
theme.fg_urgent  = "#a0524f"
theme.bg_normal  = "#2f2f2f"
theme.bg_focus   = "#819a67"
theme.bg_urgent  = "#8a725d"
theme.bg_systray = theme.bg_normal

theme.useless_gap   = dpi(0)
theme.border_width  = dpi(2)
theme.border_normal = "#202020"
theme.border_focus = "#819a67"
theme.border_marked = "#a95282"

theme.titlebar_bg_focus  = "#2f2f2f"
theme.titlebar_bg_normal = "#202020"

theme.mouse_finder_color = "#60953f"

theme.menu_border_color = "#202020"
theme.menu_border_width = dpi(1)
theme.menu_height = dpi(15)
theme.menu_width  = dpi(100)

return theme
