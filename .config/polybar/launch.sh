#!/bin/bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar, using default config location ~/.config/polybar/config
case "$1" in
    i3) exec polybar -r i3bar ;;
    xmonad) exec polybar -r xmonad ;;
    openbox) exec polybar -r openbox ;;
    bspwm) exec polybar -r bspwm ;;
    *) exec polybar -r hlwm ;;
esac
