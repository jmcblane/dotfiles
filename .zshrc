# source $HOME/.profile
alias egwtool="/home/jake/scripts/nimegw/nimgwtool"
alias voidpi="ssh -p 4002 jake@voidpi"
alias define="sdcv"
alias extract="dtrx"
alias ddgr="ddgr -n3"
alias dict="dict -d gcide"
alias sacc="/home/jake/scripts/sacclaunch.sh"
alias hc="herbstclient"
alias ls='ls -vF --color=auto --group-directories-first'
alias ll='ls -vgGFh --time-style=+"│" --color=auto --group-directories-first'
alias la='ls -vlGAFh --color=auto --group-directories-first'
alias mountiso='sudo mount -t iso9660 -o loop'
alias mutt="neomutt"
alias rm="trash"
alias xs="xbps-query -Rs"
alias xr="sudo xbps-remove -R"
alias fzf="fzf --height=25% --reverse"
alias dotgit="git --git-dir=$HOME/.dotfiles --work-tree=$HOME"
alias weather="curl wttr.in"
alias whatismyip="curl -4 ifconfig.co"
alias updatenews="cp /home/jake/docs/news_feed.xml /home/jake/voidpi/www/ && chmod +r /home/jake/voidpi/www/news_feed.xml"

function 0x0 { curl -F"file=@$@" https://0x0.st }

#################
# Misc Settings #
#################

zstyle ':completion:*' group-name ''
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' completer _expand _complete _match _prefix
zstyle :compinstall filename '/home/jake/.zshrc'
autoload -Uz compinit
compinit

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=3000
export KEYTIMEOUT=1

bindkey -v

# . /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# . $HOME/.config/zsh/plugins/bd/bd.zsh

##########
# PROMPT #
##########

setopt prompt_subst
autoload colors zsh/terminfo
autoload -Uz vcs_info

if [[ "$terminfo[colors]" -ge 8 ]]; then
    colors
fi
for color in black red green yellow blue magenta cyan white; do
    eval col_$color='%{$terminfo[bold]$fg[${(L)color}]%}'
    eval col2_$color='%{$fg[${(L)color}]%}'
    (( count = $count + 1 ))
done
nocolor="%{$terminfo[sgr0]%}"

if [ "$TERM" != "linux" ]; then
    zle-keymap-select () {
        if [ $KEYMAP = vicmd ]; then
            printf "\033[2 q"
        else
            printf "\033[4 q"
        fi
    }

    zle -N zle-keymap-select

    zle-line-init () {
        zle -K viins
        printf "\033[4 q"
    }

    zle -N zle-line-init
fi

zstyle ':vcs_info:*' actionformats '%b %a'
zstyle ':vcs_info:*' formats '%b %a'
zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b:%r'

zstyle ':vcs_info:*' enable git cvs svn

vcs_info_wrapper() {
  vcs_info
  if [ -n "$vcs_info_msg_0_" ]; then
    echo "%{$reset_color%}%{$fg[blue]%}%{$bg[black]%}  ${vcs_info_msg_0_}%{$reset_color%}$del"
  fi
}

PROMPT=$'%{\e[48;5;12m%}%{\e[38;5;232m%}  %~  $(vcs_info_wrapper)%{$reset_color%}%{\e[38;5;12m%}
$nocolor '
PS2="${col2_blue} $nocolor"

###############
# Bible Shell #
###############

alias xref="/home/jake/scripts/bible/cleaner.py"
alias search="diatheke -f plain -b KJV -s multiword -k"
alias search+="diatheke -f plain -b KJV -o n -s multiword -k"
alias search++='/home/jake/scripts/bible/search++.rb'

alias kjv+="diatheke -b KJV -f plain -o n -k"
alias greek="diatheke -b StrongsGreek -f plain -k"
alias hebrew="diatheke -b StrongsHebrew -f plain -k"
alias hitchcock="diatheke -b Hitchcock -f plain -k"
alias tsk="diatheke -b TSK -f plain -k"
alias ylt="diatheke -b YLT -f plain -k"

function mhc { diatheke -f html -b MHC -k $@ | elinks -dump | nvimpager }
function barnes { diatheke -f html -b Barnes -k $@ | elinks -dump | nvimpager }

alias genesis='b genesis'
alias exodus='b exodus'
alias psalm='b psalm'
alias proverbs='b proverbs'
alias leviticus='b leviticus'
alias numbers='b numbers'
alias deuteronomy='b deuteronomy'
alias joshua='b joshua'
alias judges='b judges'
alias ruth='b ruth'
alias 1samuel='b 1samuel'
alias 2samuel='b 2samuel'
alias 1kings='b 1kings'
alias 2kings='b 2kings'
alias 1chronicles='b 1chronicles'
alias 2chronicles='b 2chronicles'
alias ezra='b ezra'
alias nehemiah='b nehemiah'
alias esther='b esther'
alias job='b job'
alias ecclesiastes='b ecclesiastes'
alias songofsolomon='b songofsolomon'
alias isaiah='b isaiah'
alias jeremiah='b jeremiah'
alias lamentations='b lamentations'
alias ezekiel='b ezekiel'
alias daniel='b daniel'
alias hosea='b hosea'
alias joel='b joel'
alias amos='b amos'
alias obadiah='b obadiah'
alias jonah='b jonah'
alias micah='b micah'
alias nahum='b nahum'
alias habakkuk='b habakkuk'
alias zephaniah='b zephaniah'
alias haggai='b haggai'
alias zechariah='b zechariah'
alias malachi='b malachi'
alias matthew='b matthew'
alias mark='b mark'
alias luke='b luke'
alias john='b john'
alias acts='b acts'
alias romans='b romans'
alias 1corinthians='b 1corinthians'
alias 2corinthians='b 2corinthians'
alias galatians='b galatians'
alias ephesians='b ephesians'
alias philippians='b philippians'
alias colossians='b colossians'
alias 1thessalonians='b 1thessalonians'
alias 2thessalonians='b 2thessalonians'
alias 1timothy='b 1timothy'
alias 2timothy='b 2timothy'
alias titus='b titus'
alias philemon='b philemon'
alias hebrews='b hebrews'
alias james='b james'
alias 1peter='b 1peter'
alias 2peter='b 2peter'
alias jude='b jude'
alias revelation='b revelation'
alias 1john='b 1john'
alias 2john='b 2john'
alias 3john='b 3john'


#########################
# Linux Terminal Colors #
#########################
if [ "$TERM" = "linux" ]; then
    # echo -en "\e]P01a1a1a"
    # echo -en "\e]P83a3a3a" #darkgrey
    # echo -en "\e]P18f5d4e" #darkred
    # echo -en "\e]P98f6f65" #red
    # echo -en "\e]P2678f3e" #darkgreen
    # echo -en "\e]PA7a8f65" #green
    # echo -en "\e]P3888f32" #brown
    # echo -en "\e]PB8c8f32" #yellow
    # echo -en "\e]P444578f" #darkblue
    # echo -en "\e]PC65708f" #blue
    # echo -en "\e]P57a3552" #darkmagenta
    # echo -en "\e]PD715464" #magenta
    # echo -en "\e]P6458f8f" #darkcyan
    # echo -en "\e]PE658f8f" #cyan
    # echo -en "\e]P77a7075" #lightgrey
    # echo -en "\e]PFc0c0c0" #white

    # LIGHT
    # echo -en "\e]P0EDEDED"
    # # echo -en "\e]P8969694" #darkgrey
    # echo -en "\e]P87a7a79" #darkgrey
    # echo -en "\e]P1D7005F" #darkred
    # echo -en "\e]P9D7005F" #red
    # echo -en "\e]P2718C00" #darkgreen
    # echo -en "\e]PA718C00" #green
    # echo -en "\e]P3D75F00" #brown
    # echo -en "\e]PBD75F00" #yellow
    # echo -en "\e]P44271AE" #darkblue
    # echo -en "\e]PC4271AE" #blue
    # echo -en "\e]P58959A8" #darkmagenta
    # echo -en "\e]PD8959A8" #magenta
    # echo -en "\e]P63E999F" #darkcyan
    # echo -en "\e]PE3E999F" #cyan
    # echo -en "\e]P74D4D4C" #lightgrey
    # echo -en "\e]PFF5F5F5" #white

    # ArchLabs
    # echo -en "\e]P022242a"
    # echo -en "\e]P82b303b" #darkgrey
    # echo -en "\e]P1b93434" #darkred
    # echo -en "\e]P9b93434" #red
    # echo -en "\e]P260953f" #darkgreen
    # echo -en "\e]PA60953f" #green
    # echo -en "\e]P3ba4a13" #brown
    # echo -en "\e]PBba4a13" #yellow
    # echo -en "\e]P44271ae" #darkblue
    # echo -en "\e]PC4271ae" #blue
    # echo -en "\e]P56c4362" #darkmagenta
    # echo -en "\e]PD6c4362" #magenta
    # echo -en "\e]P6256163" #darkcyan
    # echo -en "\e]PE256163" #cyan
    # echo -en "\e]P78a8a8a" #lightgrey
    # echo -en "\e]PF8fa1b3" #white
    
    # Minty
    echo -en "\e]P01a1c1e"
    echo -en "\e]P83a3a3a" #darkgrey
    echo -en "\e]P1a0524f" #darkred
    echo -en "\e]P9a0524f" #red
    echo -en "\e]P2819a67" #darkgreen
    echo -en "\e]PA819a67" #green
    echo -en "\e]P3bf864f" #brown
    echo -en "\e]PB8a725d" #yellow
    echo -en "\e]P45a92af" #darkblue
    echo -en "\e]PC4f63a4" #blue
    echo -en "\e]P5775cac" #darkmagenta
    echo -en "\e]PDa95282" #magenta
    echo -en "\e]P651897e" #darkcyan
    echo -en "\e]PE51897e" #cyan
    echo -en "\e]P7929292" #lightgrey
    echo -en "\e]PFcdcdcd" #white
    clear #for background artifacting
fi

######
# EOF
